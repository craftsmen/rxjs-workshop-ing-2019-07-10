import { Subject, bufferCount, concat, debounceTime, distinctUntilChanged, filter, interval, map, mapTo, merge, of, publishReplay, reduce, refCount, scan, skip, startWith, take, withLatestFrom, zip } from '../lib/rxjs'; // tslint:disable-line:max-line-length
import { checkSolution } from '../lib/solution-checker/index';

// ASSIGNMENT: Create a Subject and make sure that the program prints the following output:
//  - after 1 second: RxJava is cool :)
//  - after 2 seconds: So reactive!
//  - after 3 seconds: Much stream!
//  - after 4 seconds: Goodbye!
// You are only allowed to modify the lines with the ??? comment. Do not change the other statements.
//
// HINT: Think of which kind of events your subject need to produce the desired output.
//
// HINT: If you look at the `checkSolution` line you'll see that "Goodbye!" will be appended after the subject 'completes', so think about
// which event you'll need to use for the 4th `setTimeout`.

const subject = new Subject<string>();

setTimeout(() => {
    // ???
}, 1000);

setTimeout(() => {
    // ???
}, 2000);

setTimeout(() => {
    // ???
}, 3000);

setTimeout(() => {
    // ???
}, 4000);

checkSolution(concat(subject, of('Goodbye!')));
