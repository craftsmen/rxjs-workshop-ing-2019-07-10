import { Subject, bufferCount, concat, debounceTime, distinctUntilChanged, filter, interval, map, mapTo, merge, of, publishReplay, reduce, refCount, scan, skip, startWith, take, withLatestFrom, zip } from '../lib/rxjs'; // tslint:disable-line:max-line-length
import { number$ } from '../lib/example-streams';
import { checkSolution } from '../lib/solution-checker/index';

// ASSIGNMENT: Create a new stream called `evenNumber$`, which is based on the number$ stream and only emits even numbers.
//
// HINT: Starting with this exercise you no longer have to subscribe to your observable stream yourself. Instead the solution is
// automatically checked for correctness through the `checkSolution` function, which is called at the end of the exercise. This function
// also prints out the output of your observable stream. For testing and debugging purposes you can still subscribe to your stream, however,
// to have your solution checked be sure that the `checkSolution` function is called.

const evenNumber$ = number$.pipe(
    // ???
);

// If implemented correctly, the application will output the following numbers: 4, 6, 2, 2, 4, 8

checkSolution(evenNumber$);
