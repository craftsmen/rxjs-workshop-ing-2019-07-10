
import { someNumber$ } from '../lib/example-streams';

// The someNumber$ stream (imported above) will emit short a sequence of numbers with an interval of 0.5 seconds.
//
// ASSIGNMENT: Subscribe to the someNumber$ stream and print each number to the console.

// someNumber$.???
