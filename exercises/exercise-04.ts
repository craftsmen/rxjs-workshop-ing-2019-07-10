import { Subject, bufferCount, concat, debounceTime, distinctUntilChanged, filter, interval, map, mapTo, merge, of, publishReplay, reduce, refCount, scan, skip, startWith, take, withLatestFrom, zip } from '../lib/rxjs'; // tslint:disable-line:max-line-length
import { number$ } from '../lib/example-streams';
import { isPrime } from '../lib/utils';
import { checkSolution } from '../lib/solution-checker/index';

// ASSIGNMENT: Use the number$ stream to define a new stream that only contains those numbers that are prime number.
//
// HINT: You can make use of the utility function isPrime to check if a given number is a prime number. You DON'T have to write your own
// function to check whether a number is a prime.

const primeNumber$ = number$.pipe(
    // ???
);

// If implemented correctly, the application will output the following numbers: 1, 7, 2, 2, 7, 3

checkSolution(primeNumber$);
