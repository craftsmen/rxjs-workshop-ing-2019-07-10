import { Subject, bufferCount, concat, debounceTime, distinctUntilChanged, filter, interval, map, mapTo, merge, of, publishReplay, reduce, refCount, scan, skip, startWith, take, withLatestFrom, zip } from '../lib/rxjs'; // tslint:disable-line:max-line-length
import { point$ } from '../lib/example-streams';
import { checkSolution } from '../lib/solution-checker/index';

// The point$ stream represents the number of earned points by your favorite sports team for each played game during last season.

// ASSIGNMENT: Use the point$ stream to display the number of points for each game, including the total number of
// points earned so far. The output should be formatted as following:
//
//   Points: 5 - total: 11
//
// HINT: You will need to use `zip` (static creation operator) and `scan` (pipeable-operator).

const subtotals$ = point$; // ???

// If implemented correctly, the application should display the following output:
//
// Points: 0 - total: 0
// Points: 3 - total: 3
// Points: 0 - total: 3
// Points: 3 - total: 6
// Points: 1 - total: 7
// Points: 3 - total: 10
// Points: 0 - total: 10
// Points: 0 - total: 10
// Points: 3 - total: 13
// Points: 0 - total: 13

checkSolution(subtotals$);
