import { Subject, bufferCount, concat, debounceTime, distinctUntilChanged, filter, interval, map, mapTo, merge, of, publishReplay, reduce, refCount, scan, skip, startWith, take, withLatestFrom, zip } from '../lib/rxjs'; // tslint:disable-line:max-line-length
import { number$ } from '../lib/example-streams';
import { checkSolution } from '../lib/solution-checker/index';

// ASSIGNMENT: create a new stream that emits the square of each number emitted by the number$ stream.

const squaredNumber$ = number$.pipe(
    // ???
);

// If implemented correctly, the application will output the following numbers: 1, 81, 16, 49, 36, 4, 4, 49, 9, 16, 64

checkSolution(squaredNumber$);
