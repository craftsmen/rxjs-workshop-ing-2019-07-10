import { Observable, MonoTypeOperatorFunction, interval as __interval } from 'rxjs';
import { debounceTime as __debounceTime } from 'rxjs/operators';
export { Subject, concat, merge, of, zip } from 'rxjs';
export { bufferCount, distinctUntilChanged, filter, map, mapTo, max, publishReplay, refCount, skip, startWith, take, withLatestFrom } from 'rxjs/operators'; // tslint:disable-line:max-line-length

export { reduce, scan } from '../lib/patched-rxjs-operators';

import { virtualTime } from './virtual-time-scheduler';

export function interval(period: number): Observable<number> {
    return __interval(period, virtualTime);
}

export function debounceTime<T>(dueTime: number): MonoTypeOperatorFunction<T> {
    return __debounceTime<T>(dueTime, virtualTime);
}
